#Customer Manager App

It is a Mean Stack Application build using Node.js, Angular JS and Mongo DB.It is a single page application developed using NPM and bower.It shows each customer products, there unit prices and total price.

##Tools Required
```
Git Bash
Node.js
MongoDB
RoboMongo
WebStorm IDE
postman
```
##Node Modules Used:
```
express
body-parser
chalk
consolidate
glob
lodash
mongoose
swig (Template Engine)
```

##Instructions to install Node Modules, Bower Dependencies and run the application in browser.

Install Git Bash and Run the following commands in Git Bash command prompt.
```
cd customer-managerApp/
npm install
bower install
node server.js
```
* Open new Git Bash cmd and change the directory to your MongoDB folder.

* Run the following commands.
```
./mongod
./mongo
```
* Open RoboMongo and Create a DB Connection with customer and product.

* Open browser and follow the below steps to use  App.
```
URL : http://localhost:3000/
Click customers to add new customer.
Click order to list all the customer.